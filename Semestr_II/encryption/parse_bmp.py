def parse_bmp(data):
    offset = data[0xa]
    return (data[:offset], data[offset:])

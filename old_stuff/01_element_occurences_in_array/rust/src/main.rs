extern crate rand;

use std::io::{self, Write};
use std::str::FromStr;
use std::error::Error;
use rand::Rng;

fn main() {
	let n: i32 = get_user_input("n").expect("Couldn't get user input");
	let k: i32 = get_user_input("k").expect("Couldn't get user input");
	let x: i32 = get_user_input("x").expect("Couldn't get user input");

	let mut occurences = 0;
	for _ in 0..n {
		let number = rand::thread_rng().gen_range(1, k + 1);
		if number == x {
			occurences += 1;
		}
	}

	println!("x occured {} times in the array", occurences);
}

fn get_user_input<T: 'static>(prompt: &str) -> Result<T, Box<Error>>
	where T: FromStr,
	<T as FromStr>::Err: Error,
{
	print!("{}: ", prompt);
	io::stdout().flush()?;

	let mut input = String::new();
	io::stdin().read_line(&mut input)?;
	let input = input.trim().parse()?;

	Ok(input)
}
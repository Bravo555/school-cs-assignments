#!/usr/bin/python3
import sys
import caesar
import transposition
from parse_bmp import parse_bmp

usage = 'Usage: {} (caesar|transposition|pad|rsa) [filename]'.format(sys.argv[0])


def main():
    if len(sys.argv) < 3:
        print("Not enough arguments!\n" + usage)
        sys.exit(1)

    cipher = sys.argv[1]

    filename, mode = None, None
    if len(sys.argv) == 3:
        if sys.argv[2] in ['encrypt', 'decrypt']:
            mode = sys.argv[2]
        else:
            filename = sys.argv[2]

    if len(sys.argv) == 4:
        filename, mode = sys.argv[2], sys.argv[3]

    texts = []
    data = None
    if filename != None:
        if filename.endswith('.bmp'):
            with open(filename, 'rb') as f:
                data = f.read()
        else:
            with open(filename) as f:
                texts = f.readlines()
    else:
        texts = sys.stdin

    if data == None:
        for text in texts:
            if cipher == 'caesar':
                if mode == 'encrypt':
                    print(caesar.caesar(text, 3))
                elif mode == "decrypt":
                    print(caesar.caesar(text, -3))
                else:
                    print("Bad mode!\n" + usage)
                    sys.exit(1)

            elif cipher == 'transposition':
                print(transposition.transposition(text))
            else:
                print('Bad cipher!')
    else:
        headers, data = parse_bmp(data)
        if cipher == 'caesar':
            if mode == 'encrypt':
                encrypted = headers + caesar.caesar_bin(data, 3)
                with open('output.bmp', 'wb') as f:
                    f.write(encrypted)
            elif mode == "decrypt":
                print(headers + caesar.caesar_bin(data, -3))
            else:
                print("Bad mode!\n" + usage)
                sys.exit(1)
        elif cipher == 'transposition':
            if mode == 'encrypt':
                encrypted = headers + transposition.transposition_bin(data)
                with open('output_transposition.bmp', 'wb') as f:
                    f.write(encrypted)


if __name__ == '__main__':
    main()

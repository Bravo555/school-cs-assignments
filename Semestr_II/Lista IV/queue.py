from node import Node


class Queue:
    def __init__(self):
        self.tail = self.head = None

    def append(self, elem):
        if self.head is None:
            self.tail = self.head = Node(elem, None)
        else:
            self.tail.next = Node(elem, None)
            self.tail = self.tail.next

    def pop(self):
        if self.head is None:
            return None
        else:
            elem = self.head.elem
            self.head = self.head.next
            return elem


def test_queue():
    queue = Queue()
    queue.append(1)
    queue.append(2)
    queue.append(3)

    assert queue.pop() == 1
    assert queue.pop() == 2

    queue.append(4)
    queue.append(5)

    assert queue.pop() == 3
    assert queue.pop() == 4
    assert queue.pop() == 5

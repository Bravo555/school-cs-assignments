#include <vector>
#include "Punkt.hpp"
#include "Figura.hpp"
#include "Trojkat.hpp"

// FORMAT: podstawy: AB, CD
class Trapez : public Figura {
public:
	Trapez(std::vector<Punkt> listaWierzcholkow) : Figura(listaWierzcholkow) {}

	float pole() {
		Trojkat first({listaWierzcholkow[0], listaWierzcholkow[1], listaWierzcholkow[3]}),
			second({listaWierzcholkow[1], listaWierzcholkow[2], listaWierzcholkow[3]});

		return first.pole() + second.pole();
	}
};

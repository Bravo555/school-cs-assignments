import math as maths

data = {
    's1': [],
    's2': [],
    's3': []
}


def load_station_data(station, base, filename):
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            (clock, temp) = line.split()
            (clock, temp) = (int(clock, base=base), int(temp, base=base))
            data[station].append({ 'clock': clock, 'temp': temp })

load_station_data('s1', 2, 'dane_systemy1.txt')
load_station_data('s2', 4, 'dane_systemy2.txt')
load_station_data('s3', 8, 'dane_systemy3.txt')

# print(data)

# lowest temp, write as binary
def lowest_temp(data):
    lowest = data[0]['temp']
    for reading in data:
        if reading['temp'] < lowest:
            lowest = reading['temp']
    return lowest

print(bin(lowest_temp(data['s1'])))
print(bin(lowest_temp(data['s2'])))
print(bin(lowest_temp(data['s3'])))

# clock value not mod 12 for all stations at once
def is_clock_valid(reading):
    return (reading['clock'] + 12) % 24 == 0

invalid_readings = 0
for (r0, r1, r2) in zip(data['s1'], data['s2'], data['s3']):
    if is_clock_valid(r0) == is_clock_valid(r1) == is_clock_valid(r2) == False:
        invalid_readings += 1
print(invalid_readings)

# num of record days
# first day is always a record
record_days = 1
record1 = data['s1'][0]['temp']
record2 = data['s2'][0]['temp']
record3 = data['s3'][0]['temp']
for (r1, r2, r3) in zip(data['s1'], data['s2'], data['s3']):
    record = False
    if r1['temp'] > record1:
        record1 = r1['temp']
        record = True
    if r2['temp'] > record2:
        record2 = r2['temp']
        record = True
    if r3['temp'] > record3:
        record3 = r3['temp']
        record = True

    if record:
        record_days += 1

print(record_days)

# biggest temp difference
biggest_diff = 0
for (i, ti) in enumerate(data['s1']):
    for (j, tj) in enumerate(data['s1']):
        rij = (ti['temp'] - tj['temp']) ** 2
        pos_diff = abs(i - j)
        if pos_diff > 0:
            temp_diff = maths.ceil(rij / pos_diff)
            if temp_diff > biggest_diff:
                biggest_diff = temp_diff

print(biggest_diff)

use std::io;
use std::cmp::Ordering;

fn main() {
    let mut n = String::new();
    io::stdin().read_line(&mut n).unwrap();
    let n: u32 = n.trim().parse().unwrap();

    println!("fib({}): {}", n, get_fibonacci_number(n));
}

fn get_fibonacci_number(n: u32) -> u32 {
    match n.cmp(&1) {
        Ordering::Less => 0,
        Ordering::Equal => 1,
        Ordering::Greater => get_fibonacci_number(n - 1) + get_fibonacci_number(n - 2)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fibonacci_works() {
        assert_eq!(5, get_fibonacci_number(5));
        assert_eq!(144, get_fibonacci_number(12));
    }
}

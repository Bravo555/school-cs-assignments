#include <cmath>
#include <iostream>
#include "Figura.hpp"
#include "Punkt.hpp"

class Kwadrat : public Figura {
public:
	Kwadrat(std::vector<Punkt> listaWierzcholkow) : Figura(listaWierzcholkow) {}

	float pole() {
		return pow(listaWierzcholkow[0].distanceFrom(listaWierzcholkow[1]), 2);
	}
};

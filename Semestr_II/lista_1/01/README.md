```
$ ./convert.py
```

Gets the numbers from standard input in the following pattern:
```
FF 16 10
```
- `FF` - Input number
- `16` - Input system
- `10` - Output system

Or you can launch it like that:
```
$ ./convert.py input.txt output.txt
```
And then it will take the input from the input file
#!/usr/bin/python3

import sys, string

def validate_input(n, input_system):
    if input_system > 36:
        raise Exception("System too big")

    input_space = ('0123456789' + string.ascii_lowercase)[:input_system]

    for digit in n:
        if digit.lower() not in input_space:
            raise Exception("Digit " + digit + " not in input space of: " + input_space)

def convert(n, input_system, desired_system):
    input_space = ('0123456789' + string.ascii_uppercase)[:desired_system]
    result = ""
    left = int(n, input_system)

    while left > 0:
        remainder = left % desired_system
        left = left // desired_system
        result = input_space[remainder] + result

    return result

def main():
    if len(sys.argv) < 3:
        (n, input_system, desired_system) = input().strip().split()
        input_system = int(input_system)
        desired_system = int(desired_system)

        try:
            # validate if input number is correct in given system
            validate_input(n, input_system)
            print(convert(n, input_system, desired_system))
        except Exception as e:
            print(e)

    else:
        (_, input_filename, output_filename) = sys.argv
        with open(input_filename, 'r') as f:
            lines = f.readlines()

        results = []
        for line in lines:
            (n, input_system, desired_system) = line.strip().split()
            input_system = int(input_system)
            desired_system = int(desired_system)

            results.append(convert(n, input_system, desired_system))
        
        with open(output_filename, 'w') as f:
            for result in results:
                f.write('{} {}\n'.format(result, desired_system))


if __name__ == '__main__':
    main()

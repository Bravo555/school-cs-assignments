#include <vector>
#include "Figura.hpp"
#include "Punkt.hpp"

#ifndef TROJKAT_H
#define TROJKAT_H

class Trojkat : public Figura {
public:
	Trojkat(std::vector<Punkt> listaWierzcholkow) : Figura(listaWierzcholkow) {}

	float pole() {
		std::vector<float> distances = {
			listaWierzcholkow[0].distanceFrom(listaWierzcholkow[1]),
			listaWierzcholkow[1].distanceFrom(listaWierzcholkow[2]),
			listaWierzcholkow[2].distanceFrom(listaWierzcholkow[0])
		};

		float p = 0;
		for(auto& it: distances) {
			p += it;
		}
		p = 0.5 * p;

		return sqrt(p * (p - distances[0]) * (p - distances[1]) * (p - distances[2]));
	}
};

#endif

#!/usr/bin/python3

import string


def caesar(text, offset):
    text = text.strip().upper()
    n = ''
    for char in text:
        n += string.ascii_uppercase[string.ascii_uppercase.index(char) + offset]
    return n


def caesar_bin(data, offset=3):
    return bytes([(b + offset) % 256 for b in data])

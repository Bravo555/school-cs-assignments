#include <iostream>

using namespace std;

int main()
{
	int n;
	float k;

	cout << "n: ";
	cin >> n;

	cout << "k: ";
	cin >> k;

	float sum = 0, number;
	for(int i = 0; i < n; i++) {
		cin >> number;
		if(number > -k && number < k) {
			sum += number;
		}
		else {
			cout << "Error: outside of specified range: <" << -k << ", " << k << ">." << endl;
			i--;
		}
	}
	const float avg = sum / n;

	cout << "Srednia: " << avg << '\n';
}
use std::collections::HashMap;
use std::io;

fn main() {
    let mut frequencies = HashMap::new();
    let mut input_text = String::new();
    io::stdin().read_line(&mut input_text).expect("Couldn't get user input");
    let input_text = input_text.trim().to_lowercase();

    for character in input_text.chars() {
        let count = frequencies.entry(character).or_insert(0);
        *count += 1;
    }

    println!("{:?}", frequencies);
}

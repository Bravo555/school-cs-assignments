#include <iostream>
#include <vector>
#include "Kwadrat.hpp"
#include "Punkt.hpp"
#include "Trojkat.hpp"
#include "Trapez.hpp"

int main() {
	std::vector<Punkt> points {
		Punkt({0.0, 0.0}), Punkt({0.0, 2.0}),
		Punkt({2.0, 2.0}), Punkt({2.0, 0.0})
	};
	Kwadrat a(points);
	Trojkat b({ Punkt({0.0, 0.0}), Punkt({0.0, 2.0}), Punkt({2.0, 0.0}) });
	Trapez c({ Punkt({0.0, 0.0}), Punkt({5.0, 0.0}), Punkt({4.0, 3.0}), Punkt({1.0, 3.0}) });

	std::cout << a.pole() << ' ' << a.obwod() << '\n' <<
		b.pole() << ' ' << b.obwod() << '\n' <<
		c.pole() << ' ' << c.obwod() << '\n';
}

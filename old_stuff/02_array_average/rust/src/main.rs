use std::io::{self, Write};

fn main() {
	let mut n = String::new();
	print!("n: ");
	io::stdout().flush();
	io::stdin().read_line(&mut n).expect("Failed to read line");
	let n : u32 = n.trim().parse().expect("Please type a number!");

	let mut k = String::new();
	print!("k: ");
	io::stdout().flush();
	io::stdin().read_line(&mut k).expect("Failed to read line");
	let k : u32 = k.trim().parse().expect("Please type a number!");

	let mut sum: f64 = 0.0;
	for i in 0..n {
		let mut number = String::new();
		io::stdin().read_line(&mut number).expect("Failed to read line");
		let number: f64 = number.trim().parse().expect("Please type a number!");

		if number <= k as f64 && number >= -(k as f64) {
			sum += number;
		} else {
			panic!("Input outside of specified range!");
		}
	}
	let avg = sum / n as f64;

	println!("Average: {}", avg);
}


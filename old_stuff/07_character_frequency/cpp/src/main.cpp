#include <iostream>
#include <map>

std::map<char, int> getCharacterFrequency(const std::string& text)
{
	std::string charset = "abcdefghijklmnopqrstuvwxyz";
	std::map<char, int> frequencies;

	// create all keys
	for(auto& it: charset) {
		frequencies[it] = 0;
	}

	for(auto& character : text) {
		auto characterLowercase = (char)tolower(character);
		if(frequencies.find(characterLowercase) != frequencies.end()) { // check if key exists
			frequencies[characterLowercase] += 1;
		}
	}

	return frequencies;
}

int main()
{
	std::string inputText;
	std::getline(std::cin, inputText);
	std::cout << inputText << "\n";

	std::map<char, int> frequencies = getCharacterFrequency(inputText);

	for(auto& it : frequencies) {
		std::cout << it.first << ": " << it.second << "\t";
	}
	std::cout << std::endl;
}

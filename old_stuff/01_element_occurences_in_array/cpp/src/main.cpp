#include <iostream>
#include <random>

using namespace std;

int main()
{
	int k, n, x;
	cout << "Podaj k: ";
	cin >> k;

	cout << "Podaj n: ";
	cin >> n;

	cout << "Podaj x: ";
	cin >> x;

	int* array = new int[n];
	
	// generating array and checking occurences because why 2 loops
	random_device generator;
	uniform_int_distribution<int> distribution(1, k);

	cout << "Tablica: ";
	int occurs = 0;
	for(int i = 0; i < n; i++) {
		array[i] = distribution(generator);
		if(array[i] == x) {
			occurs++;
		}

		cout << array[i] << " ";
	}
	cout << "\n\n";

	cout << x << " występuje w tablicy " << occurs << " razy.\n";
}
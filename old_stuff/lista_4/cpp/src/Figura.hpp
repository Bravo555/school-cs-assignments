#include <vector>
#include "Punkt.hpp"
#include <iostream>

#ifndef FIGURA_H
#define FIGURA_H

class Figura {
protected:
	std::vector<Punkt> listaWierzcholkow;

public:
	Figura(std::vector<Punkt> listaWierzcholkow) {
		this->listaWierzcholkow = listaWierzcholkow;
	}

	float obwod() {
		float sum = 0;
		for(unsigned int i = 0; i < listaWierzcholkow.size(); i++) {
			sum += listaWierzcholkow[i].distanceFrom(listaWierzcholkow[(i + 1) % listaWierzcholkow.size()]);
		}
		return sum;
	}

	virtual float pole() = 0;
};

#endif

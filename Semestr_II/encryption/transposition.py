#!/usr/bin/python3


def transposition(input):
    n = 0
    while n**2 < len(input):
        n += 1
    while len(input) < n**2:
        input += '.'

    ciphertext = ''
    for column in range(n):
        for row in range(n):
            ciphertext += input[column + n * row]

    return ciphertext


def transposition_bin(data):
    data = bytearray(data)
    encrypted = bytearray()
    n = 0
    while n ** 2 < len(data):
        n += 1
    print(n)

    while len(data) < n**2:
        data += b'\x00'

    for column in range(n):
        for row in range(n):
            encrypted += bytes(data[column + n * row])

    return encrypted

#ifndef PUNKT_H
#define PUNKT_H

#include <vector>
#include <cmath>

class Punkt {
private:
	std::vector<float> listaWspolrzednych;
	int liczbaWymiarow;

public:
	Punkt(const std::vector<float> listaWspolrzednych) {
		this->listaWspolrzednych = listaWspolrzednych;
		this->liczbaWymiarow = listaWspolrzednych.size();
	}

	Punkt(const Punkt& punkt) {
		this->listaWspolrzednych = punkt.listaWspolrzednych;
		this->liczbaWymiarow = punkt.liczbaWymiarow;
	}

	int getDimenstions() {
		return liczbaWymiarow;
	}

	void setListaWspolrzednych(std::vector<float> listaWspolrzednych) {
		this->listaWspolrzednych = listaWspolrzednych;
		this->liczbaWymiarow = listaWspolrzednych.size();
	}

	std::vector<float> getListaWspolrzednych() {
		return listaWspolrzednych;
	}

	float distanceFrom(const Punkt& point) {
		float distanceX = abs(listaWspolrzednych[0] - point.listaWspolrzednych[0]);
		float distanceY = abs(listaWspolrzednych[1] - point.listaWspolrzednych[1]);
	
		return sqrt(pow(distanceX, 2) + pow(distanceY, 2));
	}
};

#endif

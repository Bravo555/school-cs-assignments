#!/usr/bin/python3


def get_primes(n):
    numbers = [i for i in range(2, n)]

    for i in numbers:
        for j in numbers:
            if j % i == 0 and j != i:
                numbers.remove(j)

    return numbers


if __name__ == '__main__':
    n = int(input())

    print(get_primes(n))

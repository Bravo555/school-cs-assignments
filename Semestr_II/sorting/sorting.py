#!/usr/bin/python3

import math


def insertion_sort(l):
    l = l.copy()
    i = 1
    while i < len(l):
        j = i
        while j > 0 and l[j-1] > l[j]:
            l[j], l[j-1] = l[j-1], l[j]
            j -= 1
        i += 1

    return l


def selection_sort(l):
    l = l.copy()

    for i in range(len(l)):
        currentMin = i
        for j in range(i, len(l)):

            if l[j] < l[currentMin]:
                currentMin = j

        if currentMin != i:
            l[currentMin], l[i] = l[i], l[currentMin]

    return l


def mergesort(l):
    def merge(left, right):
        result = []
        i, j = 0, 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                result.append(left[i])
                i += 1
            else:
                result.append(right[j])
                j += 1

        while i < len(left):
            result.append(left[i])
            i += 1
        while j < len(right):
            result.append(right[j])
            j += 1

        return result

    if len(l) < 2:
        return l[:]

    else:
        mid = len(l) // 2
        left = mergesort(l[:mid])
        right = mergesort(l[mid:])
        return merge(left, right)


def quicksort(l):
    less = []
    equal = []
    greater = []

    if len(l) > 1:
        pivot = l[0]
        for i in l:
            if i < pivot:
                less.append(i)
            elif i > pivot:
                greater.append(i)
            elif i == pivot:
                equal.append(i)

        return quicksort(less) + equal + quicksort(greater)
    else:
        return l


if __name__ == '__main__':
    a = [8, 2, 0, -5, 7, 1, 5, 3, 4]
    print(insertion_sort(a))
    print(selection_sort(a))
    print(mergesort(a))
    print(quicksort(a))
    print(a)

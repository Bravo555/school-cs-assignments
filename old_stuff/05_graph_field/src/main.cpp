#include <iostream>
#include <vector>
#include <random>
#include <utility>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

const float X_RANGE = 20.0;
const float Y_RANGE = 10.0;

float f(float x) {
	return 0.5 * x;
}

int main(int argc, char** argv)
{
	if(argc != 2) {
		cout << "Usage: " << argv[0] << " [guesses]" << endl;
		exit(1);
	}

	std::random_device seed;
	std::mt19937 generator(seed());
	std::uniform_real_distribution<float> xRange(0, X_RANGE);
	std::uniform_real_distribution<float> yRange(0, Y_RANGE);

	const int n = atoi(argv[1]);

	int correctGuesses = 0, guesses = 0;
	for(; guesses < n; guesses++) {
		// randomize point
		std::pair<double, double> point = { xRange(generator), yRange(generator) };

		// check value for function at given x
		double functionValue = f(point.first);

		// compare y in point and computed for x
		if(point.second <= functionValue) {
			correctGuesses++;
		}
	}

	const long double fieldRatio = (float)correctGuesses / (float)guesses;
	cout << correctGuesses << " / " << guesses << " = " << fieldRatio << '\n'
		 << "Graph field: " <<  X_RANGE * Y_RANGE * fieldRatio << endl;
}

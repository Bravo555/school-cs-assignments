#include <iostream>
#include <utility>
#include <random>

using std::cout;
using std::endl;
using std::cin;;

const float RANGE = 1000.0;

int main(int argc, char** argv)
{
	if (argc != 2) {
		cout << "Usage: " << argv[0] << " [guesses]" << endl;
		exit(1);
	}
	const int n = atoi(argv[1]);

	std::random_device seed;
	std::mt19937 generator(seed());
	std::uniform_real_distribution<float> distribution(0, RANGE);

	std::pair<float, float> point = { distribution(generator), distribution(generator) };
	
	int guesses = 0, correctGuesses = 0;
	for(; guesses < n; guesses++) {
		if(point.first * point.first + point.second * point.second <= RANGE * RANGE) {
			correctGuesses++;
		}
	}

	cout << ((float)correctGuesses / (float)guesses) << endl;
}

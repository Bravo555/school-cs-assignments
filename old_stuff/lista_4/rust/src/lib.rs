#[derive(Debug)]
pub struct Point2D {
	x: f32,
	y: f32
}

impl Point2D {
	pub fn new(x: f32, y: f32) -> Point2D {
		Point2D { x, y }
	}

	pub fn from(point: &Point2D) -> Point2D {
		Point2D { x: point.x, y: point.y }
	}
}


pub trait Figure {
	fn area(&self) -> f32;
	fn obwod(&self) -> f32;
}

#[derive(Debug)]
pub struct Square<'a> {
	vertecies: &'a[Point2D; 4]
}

impl<'a> Square<'a> {
	pub fn new(vertecies: &[Point2D; 4]) -> Square {
		Square { vertecies }
	}

	pub fn length(&self) -> f32 {
		let point1 = &self.vertecies[0];
		let point2 = &self.vertecies[1];

		let (x, y);

		if point1.x > point2.x {
			x = point1.x - point2.x;
		}
		else {
			x = point2.x - point1.x;
		}
		
		if point1.y > point2.y {
			y = point1.y - point2.y;
		}
		else {
			y = point2.y - point1.y;
		}

		(x * x + y * y).sqrt()
	}
}

impl<'a> Figure for Square<'a> {
	fn area(&self) -> f32 {
		let length = self.length();
		length * length
	}

	fn obwod(&self) -> f32 {
		self.length() * 4.0
	}
}

#[derive(Debug)]
struct Triangle<'a> {
	vertecies: &'a[Point2D; 3]
}

impl Triangle {
	pub fn new(vertecies: &[Point2D; 3]) {
		Point2D {vertecies}
	}
}

impl Figure for Triangle {
	
}

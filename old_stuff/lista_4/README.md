# lista-4

Zadanie 1. Proszę zapoznać się z tekstem zamieszczonym na stronie: https://pl.wikibooks.org/wiki/C%2B%2B/Funkcje_wirtualne

Zadanie 2. Proszę zaimplementować klasę Punkt posiadającą prywatne pola listaWspolrzednych (np. vector), liczbaWymiarow, konstruktor inicjalizujący, konstruktor kopiujący oraz metody get i set. 

Zadanie 3. Proszę zaimplementować klasę Figura posiadającą pole listaWierzcholkow (zawierające vector lub listę punktów), metodę ObliczObwód, oraz wirtualną metodę ObliczPole.

Zadanie 4. Proszę zaimplementować klasę Kwadrat dziedziczącą po klasie Figura i implementującą metodę ObliczPole.

Zadanie 5. Proszę zaimplementować klasę Trójkąt dziedziczącą po klasie Figura i implementującą metodę ObliczPole.

Zadanie 6. Proszę zaimplementować klasę Trapez dziedziczącą po klasie Figura i implementującą metodę ObliczPole.

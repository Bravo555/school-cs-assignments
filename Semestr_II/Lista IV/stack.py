from node import Node


class Stack:
    def __init__(self):
        self.head = None

    def push(self, elem):
        prev = None if self.head is None else self.head
        self.head = Node(elem, prev)

    def pop(self):
        if self.head is None:
            return None
        else:
            elem = self.head.elem
            self.head = self.head.prev
            return elem


def test_stack():
    s = Stack()

    s.push(1)
    s.push(2)
    s.push(3)

    assert s.pop() == 3
    assert s.pop() == 2
    assert s.pop() == 1
    assert s.pop() == None

#include <iostream>
#include <cstdlib>

using namespace std;

unsigned long long get_fibonacci_number(int n) {
	if(n > 1) {
		return get_fibonacci_number(n - 1) + get_fibonacci_number(n - 2);
	}
	else if(n >= 0){
		return n;
	}
}

int main(int argc, char** argv)
{
	if(argc != 2) {
		cout << "Usage: fibonacci [number]" << endl;
		exit(1);
	}
	int n = std::stoi(argv[1]);

	cout << get_fibonacci_number(n) << endl;
}

extern crate oop_advanced;

use oop_advanced::*;

fn main() {
    let points = [
        Point2D::new(0.0, 0.0),
        Point2D::new(2.0, 0.0),
        Point2D::new(2.0, 2.0),
        Point2D::new(0.0, 2.0),
    ];

    let square = Square::new(&points);

    println!("{:?}", square);
    println!("{}, {}", square.area(), square.obwod());
}

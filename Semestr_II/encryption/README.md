# encryption

Input format:

- file:
    ```
    abc
    def
    jet fuel cant melt steel beams
    ```
    Execution:
    ```
    $ ./main.py caesar filename.txt encrypt
    ```

- standard input:
```
$ ./main.py transposition
KUBUŚ PUCHATEK NIE MIAŁ DZIŚ MIODKU
KPE DIUUKMZOBC IIDUHNAŚKŚAIŁ U TE M.
```
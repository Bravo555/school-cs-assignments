use std::io;
use std::collections::HashMap;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    let roman = input.trim();

    println!("{}", from_roman_to_decimal(roman));
}

fn from_roman_to_decimal(roman: &str) -> i32 {
    let roman_vec = vec!['I', 'V', 'X', 'L', 'C', 'D', 'M'];
    let decimal_vec = vec![1, 5, 10, 50, 100, 500, 1000];
    
    let decimal: HashMap<_, _> = roman_vec.iter().zip(decimal_vec.iter()).collect();

    for c in &roman {
        
    }
}

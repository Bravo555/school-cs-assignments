use std::env;
use std::fs::File;
use std::io::Read;

fn main() {
    let filename = env::args().nth(1).expect("Podaj nazwe pliku gnoju");
    let mut file = File::open(filename).unwrap();
    let mut text = String::new();
    file.read_to_string(&mut text).unwrap();
    let text = text.trim();
    println!("{}", text);

    println!("Entire text:");
    println!("{} vovels", count_vovels(&text));
    println!("{} numbers", count_numbers(&text));

    println!("Blocks:");

    let mut iter = text.chars().peekable();

    let mut blocks: Vec<_> = Vec::new();
    while let Some(_c) = iter.peek().map(|c| *c) {
        let block: String = iter.by_ref().take(20).collect();
        blocks.push(block);
    }

    let block_with_most_vovels = blocks.iter().max_by(|x, y| count_vovels(x).cmp(&count_vovels(y))).unwrap();
    let block_with_most_numbers = blocks.iter().max_by(|x, y| count_numbers(x).cmp(&count_numbers(y))).unwrap();

    println!("Block with most vovels: {} ({} vovels)", block_with_most_vovels, count_vovels(block_with_most_vovels));
    println!("Block with most numbers: {} ({} numbers)", block_with_most_numbers, count_numbers(block_with_most_numbers));
}

fn count_vovels(text: &str) -> usize {
    let vovels = "eyuioa";

    count_chars(vovels, text)
}

fn count_numbers(text: &str) -> usize {
    let numbers = "0123456789";

    count_chars(numbers, text)
}

fn count_chars(pattern: &str, text: &str) -> usize {
    text.chars().filter(|c| {
        pattern.chars().any(|p| c == &p)
    }).count()
}


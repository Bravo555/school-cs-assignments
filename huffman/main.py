#!/usr/bin/python3

import string
from operator import itemgetter
from textwrap import wrap
import sys


class Node:
    def __init__(self, char, occurences, left=None, right=None):
        self.left = left
        self.right = right
        self.char = char
        self.occurences = occurences

    def __repr__(self):
        return '({}, {})'.format(self.char, self.occurences)


def character_frequencies(text):
    result = dict()

    for char in text:
        if char in result.keys():
            result[char] += 1
        else:
            result[char] = 1

    return result


def word_frequencies(text):
    result = dict()

    for word in text.split():
        # check if the word is an actual word
        # check if all characters are letters
        if set(word).issubset(set(string.ascii_letters)):
            if word in result.keys():
                result[word] += 1
            else:
                result[word] = 1

    return result


def create_tree(frequencies):
    if len(frequencies) == 1:
        return frequencies[0]
    else:
        # take first 2 elements
        a, b = frequencies.pop(0), frequencies.pop(0)
        # fold them
        root = Node(None, a.occurences + b.occurences, left=a, right=b)
        # add so the list is still sorted
        for i in range(len(frequencies)):
            if root.occurences <= frequencies[i].occurences:
                frequencies.insert(i, root)
                break
            else:
                frequencies.append(root)
                break
        if frequencies == []:
            frequencies.append(root)
        return create_tree(frequencies)


def encode_char(char, root, accumulator=''):
    if root.left is None:
        if char != root.char:
            return None
        else:
            return accumulator
    else:
        return encode_char(char, root.left, accumulator + '0') or encode_char(char, root.right, accumulator + '1')


if __name__ == '__main__':
    try:
        filename = sys.argv[1]
    except:
        print('Usage: ./main.py filename.txt')
        sys.exit(1)
    with open(filename, 'r') as f:
        text = f.read()

    words_dict = word_frequencies(text)
    words = sorted(words_dict.items(), key=itemgetter(1), reverse=True)
    print('10 najczęstszych słów:', words[:10])

    frequencies_dict = character_frequencies(text)
    frequencies = [Node(elem[0], elem[1]) for elem in sorted(
        frequencies_dict.items(), key=itemgetter(1))]

    tree = create_tree(frequencies)

    output = ''
    for c in text:
        encoded = encode_char(c, tree)
        if encoded is None:
            raise Exception('Character can\'t be encoded!')
        else:
            output += encoded

    output = bytes([int(x, 2) for x in wrap(output, 8)])
    filename = '.'.join([filename.split('.')[0], 'bin'])
    with open(filename, 'wb') as f:
        f.write(output)

    # współczynnik kompresji
    print('Współczynnik kompresji:', len(output)/len(text.encode('utf8')))


def test_create_tree():
    text = 'aaaaaaaabbbbccd'
    frequencies_dict = character_frequencies(text)
    frequencies = [Node(elem[0], elem[1]) for elem in sorted(
        frequencies_dict.items(), key=itemgetter(1))]
    print(frequencies)
    tree = create_tree(frequencies)
    assert tree.occurences == len(text)
    assert tree.right.occurences == 8
    assert tree.left.occurences == 7
    assert tree.left.right.occurences == 4
    assert tree.left.left.occurences == 3
    assert tree.left.left.right.occurences == 2
    assert tree.left.left.left.occurences == 1
